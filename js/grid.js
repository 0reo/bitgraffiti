function Grid()
{
	var requestAnimationFrame = 	window.requestAnimationFrame || window.mozRequestAnimationFrame ||
					window.webkitRequestAnimationFrame || window.msRequestAnimationFrame;
	var obj = this;
	requestAnimationFrame = requestAnimationFrame;

	obj.canvas = document.getElementById('pixelScape');
	obj.bgcanvas = document.getElementById('checkers');
	obj.menu = document.getElementById('pickerIcons');
	obj.ctx = obj.canvas.getContext('2d');
	obj.cctx = obj.bgcanvas.getContext('2d');
	obj.settings = {
		"start":Date.now(),
		"width":640,
		"height":480,
		"zoom":10,
		"xPos":0,
		"yPos":0,
		"activeCol":{r: 0, g: 0, b: 0, a: 1},
		"bgOn":true
	};
	obj.users=[];
	obj.roomName = null;
	obj.pixels = [];
	obj.action = "draw";
	obj.offset = {x:0, y:0};
	obj.xyPix = [];

	window.onresize = function(){
		$("#pixelCount").css("z-index", 20000);
		obj.setUpGrid(true);
		obj.draw();
	};

	obj.menu.addEventListener("click", function(e){
		if (e.target.id !== obj.action)
		{
			document.getElementById(obj.action).className = "";
			e.target.className = "active";
			obj.action = e.target.id;
		}
	});

	obj.canvas.addEventListener("click", function(e){
		//console.log(parseInt(e.offsetX/obj.settings.zoom));
		if (obj.action === "draw")
		{
			xPos = parseInt((e.offsetX||e.layerX)/obj.settings.zoom)-obj.getPos().x;
			yPos = parseInt((e.offsetY||e.layerY)/obj.settings.zoom)-obj.getPos().y;

			if (xPos > obj.settings.width-1 || xPos < 0 ||
				yPos > obj.settings.height-1 || yPos < 0)
			{
				alert("You are outside the allowed drawing area.  Please move the canvas to continue drawing");
				return false;
			}

			var pix = new Pixel();
			pix.setColor(obj.getActiveColor());

			pix.setPosX(xPos);
			pix.setPosY(yPos);
			
			$.ajax({
				type:"post",
				url:"/draw",
				data:pix.settings,
				//xhr:function(){},
				dataType:"json",
				error:function(){
					console.log("connection error");

					obj.pixels.push(pix);

					obj.draw(obj.pixels.length-1);
				},
				success:function(data){
					if (data.status == "success")
					{
						pix.setDbId(data.data);
						//console.log(obj.pixels);

						/*if (!binarySearch(obj.pixels, 0, obj.pixels.length-1, data.data))
						{
							obj.pixels.push(pix);
							obj.draw();
						}*/
					}
					else if (data.status == "failure")
					{
						alert("Could not save to server.  Your changes will appear locally, but they will not be visable to others.")
						if (obj.xyPix[pix.settings.x] == undefined)
							obj.xyPix[pix.settings.x] = [];
						if (obj.xyPix[pix.settings.x][pix.settings.y] == undefined)
							obj.xyPix[pix.settings.x][pix.settings.y] = [];
						obj.xyPix[pix.settings.x][pix.settings.y].push(obj.pixels.length);

						console.log(pix);

						obj.pixels.push(pix);
						obj.draw(obj.pixels.length-1);
					}
				}
			});
		}
		else if (obj.action === "erase")
		{

			click = {x:parseInt((e.offsetX||e.layerX)/obj.getZoom())-obj.getPos().x,
				y:parseInt((e.offsetY||e.layerY)/obj.getZoom())-obj.getPos().y};

			console.log(click);

			if (obj.xyPix[click.x] === undefined || obj.xyPix[click.x][click.y] === undefined)
			{
				console.log("click area empty");
				return false;
			}

			var del = false;

			var index = obj.xyPix[click.x][click.y].pop();
			if (obj.pixels[index] === undefined)
			{
				console.log("null pixel index");
				obj.setUpGrid(false);
				obj.draw();
				return false;
			}

			$.ajax({
				type:"delete",
				url:"/pixel?id="+obj.pixels[index].dbId,
				dataType:"json",
				success:function(data, status, xhr){
					if (data.status == "success")
					{
						console.log("deleted, pixel should go away");
					}
					else if (data.status == "failure")
					{
						console.log("failed, manually removing");
						obj.pixels.splice(index, 1);

						obj.xyPix.forEach(function(x, xi, xarr){
							x.forEach(function(y, yi, yarr){
								for (var i = 0, yl = y.length; i < yl; i++)	
								{
									if (y[i] >= index)
										y[i] = y[i]-1;
								}
							})
						})
						obj.setUpGrid(false);
						obj.draw();
					}

				}
			});

				//obj.xyPix[click.x].splice(click.y, 1);
		}
		else if (obj.action === "zoomIn")
		{
			obj.setZoom(obj.settings.zoom+1);
			obj.draw();
		}
		else if (obj.action === "zoomOut")
		{
			if (obj.settings.zoom > 1)
			{
				obj.setZoom(obj.settings.zoom-1);
				obj.draw();
			}
		}
	});

	var leftButtonDown = false;
	$(obj.canvas).mousedown(function(e){
		// Left mouse button was pressed, set flag
		if(e.which === 1) leftButtonDown = true;
	});
	$(document).mouseup(function(e){
		// Left mouse button was released, clear flag
		if(e.which === 1) leftButtonDown = false;
	});

	var mousePosition = {x:-1, y:-1};
	obj.canvas.addEventListener("mousemove", function(e){
		if (obj.action !== "move")
			return false;

		if (leftButtonDown)
		{
			if (mousePosition.x !== -1)
			{
				obj.settings.xPos += Math.round((e.screenX - mousePosition.x)/obj.settings.zoom);
				obj.settings.yPos += Math.round((e.screenY - mousePosition.y)/obj.settings.zoom);
				obj.setUpGrid(false);
				obj.draw();
			}
			mousePosition.x = e.screenX;
			mousePosition.y = e.screenY;
		}
		else
		{
			mousePosition.x = -1;
			mousePosition.y = -1;
		}
		
		
	});

	document.getElementById("bgToggle").addEventListener("click", function(e){
		$toggleBtn = $(e.target);
		if ($toggleBtn.hasClass("active"))
			obj.settings.bgOn = true;
		else
			obj.settings.bgOn = false;
		$toggleBtn.toggleClass("active");
		obj.setUpGrid(true);
		obj.draw();
	});

	window.onbeforeunload = function(){
		$.ajax({
			type:"delete",
			async:false,
			cache:false,
			url:"/userRoom",
			//xhr:function(){},
			dataType:'json',
			error:function(d,x){
				console.log(x)
			},
			success:function(data){
				console.log(data);
				//if (data.status == "success")
			}
		});
	}
	obj.setUpGrid(true);

}

Grid.prototype.startGrid = function()
{
	var obj = this;
	$.ajax({
		type:"get",
		url:"/pixel",
		cache:false,
		dataType:"json",
		success:function(data, status, xhr){
			if (data.status == "success")
			{
				for (var i = 0; i < data.data.length; i++)
				{
					if (obj.pixels[i] === undefined)
					{
						obj.pixels[i] = new Pixel();
					}
					if (obj.pixels[i].settings !== data.data[i].settings)
					{
						obj.pixels[i].settings = data.data[i].settings;

						if (obj.xyPix[obj.pixels[i].settings.x] == undefined)
							obj.xyPix[obj.pixels[i].settings.x] = [];
						if (obj.xyPix[obj.pixels[i].settings.x][obj.pixels[i].settings.y] == undefined)
							obj.xyPix[obj.pixels[i].settings.x][obj.pixels[i].settings.y] = [];
						obj.xyPix[obj.pixels[i].settings.x][obj.pixels[i].settings.y].push(i);


						obj.pixels[i].settings.width = 1;
						obj.pixels[i].settings.height = 1;
					}
					if(obj.pixels[i].dbId !== data.data[i].dbId)
					{
						obj.pixels[i].dbId = data.data[i].dbId;
					}
				}
				obj.draw();
			}
		}
	});
}

Grid.prototype.setActiveColor = function(_col)
{
	var obj = this;
	obj.settings.activeCol = _col;
}

Grid.prototype.getActiveColor = function()
{
	var obj = this;
	return obj.settings.activeCol;
}

Grid.prototype.update = function(etag, last_modified)
{
	if (last_modified === undefined)
		last_modified = 0;
	if (etag === undefined)
		etag = 0;

	var obj = this;
	console.log("running update...");
	$.ajax({
		type:"get",
		url:"/sub/"+obj.roomName,
		//data:{},
		//xhr:function(){},
		dataType:"json",
		cache:false,
		beforeSend: function(xhr) 
		{ 
			xhr.setRequestHeader("If-None-Match", etag); 
			xhr.setRequestHeader("If-Modified-Since", last_modified); 
		},
		error:function(xhr, text, err){
			console.log("nothing returned from update.  trying again...");

			etag = xhr.getResponseHeader('Etag'); 
			last_modified = xhr.getResponseHeader('Last-Modified'); 
			console.log(etag);
			console.log(last_modified);



			obj.update(etag, last_modified);
		},
		success:function(data, status, xhr){
			if(xhr.status !== 200)
			{
				console.log(xhr.status+" returned from update.  trying again...");
				
				return false;
			}
			else if (xhr.status == 200)
			{
				etag = xhr.getResponseHeader('Etag'); 
				last_modified = xhr.getResponseHeader('Last-Modified'); 
				console.log("200");
				if (data.what == "userEnter")
				{
					if (data.user.user_id == obj.users[0].userId)
					{
						console.log("user match");
					}
					else
						setTimeout(function(){
							var footer = document.getElementById("userspace");//getElementsByTagName("footer")[0];
							var boxes = document.getElementsByClassName('userBoxes');
							for (var i = 1, b = boxes.length; i < b; i++)
							{
								if (boxes[i].dataset.userName == data.user.user_name &&
									boxes[i].dataset.userId == data.user.user_id)
								{
									console.log("hit! - "+data.user.user_name);
									return false;
								}
							}
							var userBox = document.createElement("div");
							var num = boxes.length+1;
							userBox.id = "u"+num;
							userBox.className = "userBoxes";
							userBox.innerHTML = data.user.user_name;
							userBox.dataset.userName = data.user.user_name;
							userBox.dataset.userId = data.user.user_id;
							footer.appendChild(userBox);
							var boxes = document.getElementsByClassName('userBoxes');
							var l = boxes.length;
							for (var i = 0; i<l; i++)
							{
								boxes[i].style.width = (100 / l)+"%";
							}

						}, 500);
				}
				else if (data.what == "userLeave")
				{

					var users = document.getElementsByClassName('userBoxes');
					var minus = false;
					for (var i = 0; i < users.length; i++)
					{
						if (users[i].dataset.userName == data.user.user_name && 
							users[i].dataset.userId == data.user.user_id)
						{
							users[i].parentElement.removeChild(users[i]);
							minus = true;
						}
						if (minus && users[i] !== undefined)
						{
							users[i].id = "u"+(i+1);
						}
					}
					var l = users.length;
					for (var i = 0; i<l; i++)
					{
						users[i].style.width = (100 / l)+"%";
					}
				}
				else if (data.what == "pixel")
				{
					if (!binarySearch(obj.pixels, 0, obj.pixels.length-1, data.dbId))
					{
						var drawStart = obj.pixels.length;

						var pix = new Pixel();

						pix.settings = data.settings;


						if (obj.xyPix[pix.settings.x] == undefined)
							obj.xyPix[pix.settings.x] = [];
						if (obj.xyPix[pix.settings.x][pix.settings.y] == undefined)
							obj.xyPix[pix.settings.x][pix.settings.y] = [];
						obj.xyPix[pix.settings.x][pix.settings.y].push(drawStart);

						pix.settings.width = 1;
						pix.settings.height = 1;

						pix.dbId = data.dbId;
						obj.pixels.push(pix);

						console.log("[update2]num of pix: "+obj.pixels.length);
						obj.draw(drawStart);
					}
				}
				else if (data.what == "delete")
				{
					console.log("delete update");
					var index = binarySearch(obj.pixels, 0, obj.pixels.length-1, data.dbId);
					if (index)
					{
						console.log("index exsists");
						obj.pixels.splice(index, 1);

						obj.setUpGrid(false);
						obj.draw();

						obj.xyPix.forEach(function(x, xi, xarr){
							x.forEach(function(y, yi, yarr){
								for (var i = 0, yl = y.length; i < yl; i++)	
								{
									if (y[i] >= index)
										y[i] = y[i]-1;
								}
							})
						})

					}
					else
					{
						console.log("no index - "+index);
						console.log(obj.pixels);
					}
				}
			}
			obj.update(etag, last_modified);
		}
	});
}

Grid.prototype.setZoom = function(_zoom)
{
	var obj = this;

	obj.settings.zoom = _zoom;

	obj.setUpGrid(true);

}

Grid.prototype.getZoom = function()
{
	var obj = this;
	return obj.settings.zoom;
}

Grid.prototype.getPos = function()
{
	var obj = this;
	return {"x":obj.settings.xPos,
		"y":obj.settings.yPos};
}

Grid.prototype.setUpGrid = function(both)
{
	var obj = this;

	obj.canvas.height = window.innerHeight - 295;
	obj.canvas.width = Math.max(document.body.offsetWidth, 1280);

	obj.ctx.scale(obj.settings.zoom, obj.settings.zoom);

	if (both)
	{
		
		obj.bgcanvas.height = obj.canvas.height;
		obj.bgcanvas.width = obj.canvas.width;

		if (obj.settings.bgOn){
			obj.cctx.scale(obj.settings.zoom, obj.settings.zoom);

			for (var x = 0; x < obj.settings.width; x++)
			{
				for (var y = 0; y < obj.settings.height; y++)
				{
					obj.cctx.fillStyle = (y+x)%2?"#f2f2f2":"#d2d2d2";
					obj.cctx.fillRect (x, y, 1, 1);
				}
			}
		}
	}
}

Grid.prototype.draw = function(_drawStart)
{
	var obj = this;

	if (!obj)
		return false;

	var pix = obj.pixels;

	if (_drawStart === undefined)
		_drawStart = 0;

	for (var i = _drawStart; i < pix.length; i++)
	{
		if (pix[i] === undefined)
			continue;
		var x = pix[i].getPosX();
		if (obj.action !== "move")
		for (var y = obj.xyPix[x].length-1; obj.settings.bgOn && y >= 0; y--)
		{
			if (obj.xyPix[x][y] !== undefined && obj.xyPix[x][y][0] == i)
			{
				obj.ctx.fillStyle = "rgb(255,255,255)";
				obj.ctx.fillRect (pix[i].getPosX()+obj.getPos().x, pix[i].getPosY()+obj.getPos().y,
					pix[i].getWidth(), pix[i].getHeight());	
			}
		}

		obj.ctx.fillStyle = "rgba("+pix[i].getColor()+")";
		obj.ctx.fillRect (pix[i].getPosX()+obj.getPos().x, pix[i].getPosY()+obj.getPos().y,
			pix[i].getWidth(), pix[i].getHeight());
	}


}

Grid.prototype.saveImg = function()
{
	var obj = this;

	if (!obj)
		return false;


	var saveCan = document.createElement("canvas");

	//saveCan.id = "u"+num;
	//saveCan.className = "userBoxes";
	//saveCan.innerHTML = data.user.user_name;
	saveCan.width = obj.settings.width;
	saveCan.height = obj.settings.height;
	//document.body.appendChild(userBox);

	var saveCtx = saveCan.getContext('2d');


	var pix = obj.pixels;

	for (var i = 0; i < pix.length; i++)
	{
		if (pix[i] === undefined)
			continue;
		var x = pix[i].getPosX();

		saveCtx.fillStyle = "rgba("+pix[i].getColor()+")";
		saveCtx.fillRect (pix[i].getPosX()+obj.getPos().x, pix[i].getPosY()+obj.getPos().y,
			1, 1);
	}

	document.getElementById("saveCanvas").download = obj.roomName;
	document.getElementById("saveCanvas").href = saveCan.toDataURL("image/png");
	document.getElementById("saveCanvas").click();
	//var image = saveCan.toDataURL("image/png");//.replace("image/png", "image/octet-stream");  // here is the most important part because if you dont replace you will get a DOM 18 exception.
	//var win=window.open(image, '_blank');
	//win.focus();
	//window.location.href="image."+image;
}

function binarySearch(_arr, start, finish, _search)
{

	if (finish < start)
		return false;

	imid = (start+finish) / 2|0;
	var current = _arr[imid].dbId;

//	console.log("dbid: "+current+" - search: "+_search+" - start: "+start+" - finish: "+finish+" - mid: "+imid);

	if (parseInt(current) > parseInt(_search))
	{
		return binarySearch(_arr, start, imid-1, _search);
	}
	else if (parseInt(current) < parseInt(_search))
	{
		return binarySearch(_arr, imid+1, finish, _search);
	}
	else
	{
		return imid;
	}

}

/*function binarySearch(_arr, start, length, _search)
{
	
	if (_arr[start] === undefined)
		console.log("dbid: "+current+" - search: "+_search+" - start: "+start+" - length: "+length);
	var current = _arr[start].dbId;
	console.log("dbid: "+current+" - search: "+_search+" - start: "+start+" - length: "+length);


	if (current === _search)
		return start;

	if (length === 0)
		return false;

	if (length % 2)
		length += 1;
	if (start % 2)
		start += 1;
	length = length / 2|0;


	if (_search < current)//lower
		return binarySearch(_arr, start-length, length, _search);
	else if (_search > current)//higher
		return binarySearch(_arr, start+length, length, _search);

	
}*/



