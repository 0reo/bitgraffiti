var pixelGrid = null;
var popup = null;



window.onload = function(){
	pixelGrid = new Grid();
	$("#colorPickerInput").spectrum({
		flat: true,
		showInput: true,
		showAlpha: true,
		localStorageKey: "bitgraffiti",
		showPalette: true,
		showSelectionPalette: true,
		clickoutFiresChange: false,
		chooseText: "Pick Colour",
		className: "colorPicker",
		preferredFormat: "rgb",
		change: function(color) {
			console.log("change");
			pixelGrid.setActiveColor(color.toRgb()); // #ff0000
		}
		//maxSelectionSize: int,
	});

	$.ajax({
		type:"get",
		url:"/checkLogin",
		dataType:"json",
		error:function(){
			
		},
		success:function(data, status, xhr){
			if (data.status == "success")
			{
				//var _last_modified = xhr.getResponseHeader('Last-Modified'); 
				login(data.data);
			}
			else if (data.status == "failure")
			{
				console.log(data);
				var loginBtn = document.getElementById("login");
				loginBtn.addEventListener("click", showSignIn);

				var pixelCount = document.createElement("div");
				pixelCount.id = "pixelCount";
				pixelCount.innerHTML = "<div style='font-size: 0.5em;margin-top: 1%;margin-bottom: 2%;'>The place to draw pixel art online!</div>"+
					"<div>"+data.data[0].pixels+"</div>"+
					"<div style='font-size: 0.4em;'>pixels drawn!</div>";
				document.getElementsByTagName("body")[0].appendChild(pixelCount);

				showSignIn();			
			}
		}
	}, "json");


	$("#invite").on("click", showRoom);
	$("#save").on("click", function(){pixelGrid.saveImg();});
	$("#rooms").on("click", getRoomList);
	$("#about").on("click", about);
	$("#help").on("click", help);

	pixelGrid.draw();
};

function showSignIn()
{

    var inputs = [];

    inputs.push({
      "id":"email",
      "name":"email",
      "type":"email",
      "placeholder":"Email Address",
      "required":"required"
    });
    inputs.push({
      "id":"password",
      "name":"password",
      "type":"password",
      "placeholder":"Password",
      "required":"required",
      "min":6
    });
    inputs.push({
      "id":"registerBtn",
      "type":"button",
      "value":"Register"
    });
    inputs.push({
      "id":"signinBtn",
	"name":"signinBtn",
      "className":"endBtn",
      "type":"submit",
      "value":"Sign In"
    });
    popup = tagLib.popupWindow(inputs, [], true);
	popup.disableClickOff();
	
    popup.on("registerBtn", "click", function(e){
      eml = document.getElementById("email").value;
      pswd = document.getElementById("password").value;
      popup.closePopup();
	var pixelCount = document.getElementById("pixelCount");
	if (pixelCount !== null)
		pixelCount.parentNode.removeChild(pixelCount);
      showRegister(eml, pswd);
    });

	popup.on("form", "submit", function(e){
        
		e.preventDefault();
		popup.clearMsg();
		$.ajax({
			type:"post",
			data:$(this).serializeArray(),
			cache:false,
			url:"/login",
			//xhr:function(){},
			dataType:'json',
			error:function(d,x){
				console.log(d)
			},
			success:function(data, status, xhr){
				console.log(data);
				if (data.status == "success")
				{
					login(data.data);
					var pixelCount = document.getElementById("pixelCount");
					pixelCount.parentNode.removeChild(pixelCount);
				}
				else
					popup.setMsg("error", data.msg)
			}
		});
	});
}

function login(_data)
{
  //<div class="userBoxes" id="u1"></div>
	pixelGrid.users[0] = _data;
	var loginBtn = document.getElementById("login");
	loginBtn.innerHTML = "Log Out";
	loginBtn.id = "logout";
	loginBtn.removeEventListener("click", showSignIn);
	loginBtn.addEventListener("click", logout);
	if (popup !== null)
		popup.closePopup();

	var footer = document.getElementById("userspace");//getElementsByTagName("footer")[0];
	userBox = document.createElement("div");
	userBox.id = "u1";
	userBox.className = "userBoxes";
	userBox.innerHTML = pixelGrid.users[0].userName;
	footer.appendChild(userBox);

	pixelGrid.roomName = _data.roomName;
	setTimeout(function(){
		pixelGrid.update(0, 'Thu, 1 Jan 1970 00:00:00 GMT')
	}, 2000);

	pixelGrid.startGrid();

	$.ajax({
		type:"get",
		url:"/users",
		data:{"roomName":pixelGrid.roomName},
		//xhr:function(){},
		dataType:"json",
		success:function(data, status, xhr){
			if (data.status == "success")
			{
				//console.log(data);
				var footer = document.getElementById("userspace");//getElementsByTagName("footer")[0];
				for (x in data.data)
				{
					userBox = document.createElement("div");
					num = document.getElementsByClassName('userBoxes').length+1;
					userBox.id = "u"+num;
					userBox.className = "userBoxes";
					userBox.innerHTML = data.data[x].username;
					userBox.dataset.userName = data.data[x].username;
					userBox.dataset.userId = data.data[x].id;
					footer.appendChild(userBox);
				}
				var userBoxes = document.getElementsByClassName('userBoxes');
				var l = userBoxes.length;
				for (var i = 0; i<l; i++)
				{
					console.log(userBoxes[i].parentElement.offsetWidth / l);
					userBoxes[i].style.width = (100 / l)+"%";
				}
			}
		}
	});
}

function logout()
{
      $.ajax({
        type:"post",
        cache:false,
        url:"/logout",
        //xhr:function(){},
        dataType:'json',
        error:function(d,x){
          console.log(d)
        },
        success:function(data){
		if (data.status == "success")
		{
			window.location.replace("/");
		}

        }
      });

}


function showRegister(eml, pswd)
{
    var inputs = [];
    inputs.push({
      "id":"username",
      "name":"username",
      "type":"text",
      "placeholder":"Username",
      "required":"required",
      "min":4
    });
    inputs.push({
      "id":"email",
      "name":"email",
      "type":"email",
      "placeholder":"Email Address",
      "value":eml,
      "required":"required"
    });
  inputs.push({
      "id":"cemail",
      "name":"cemail",
      "type":"email",
      "placeholder":"Confirm Email Address",
    "required":"required"
    });
    inputs.push({
      "id":"password",
      "name":"password",
      "type":"password",
      "placeholder":"Password",
      "value":pswd,
      "required":"required",
      "min":6
    });
    inputs.push({
      "id":"cpassword",
      "name":"cpassword",
      "type":"password",
      "placeholder":"Confirm Password",
      "required":"required",
    });
    inputs.push({
      "id":"registerBtn",
      "type":"submit",
      "value":"Register"
    });
    inputs.push({
      "id":"cancelBtn",
      "className":"endBtn",
      "type":"button",
      "value":"Cancel"
    });
    popup = tagLib.popupWindow(inputs, [], true);
	popup.disableClickOff();
    popup.on("cemail", "input", function(e){
      checkEmail(this);
    });
    popup.on("cpassword", "input", function(e){
      checkPass(this);
    });
    popup.on("cancelBtn", "click", function(e){
      popup.closePopup();
	showSignIn();
    });
    popup.on("form", "submit", function(e){
      e.preventDefault();
        
      $.ajax({
        type:"post",
        data:$(this).serializeArray(),
        cache:false,
        url:"/register",
        //xhr:function(){},
        dataType:"json",
        error:function(){
          console.log("error")
        },
        success:function(data){
		if (data.status == "success")
		{
			popup.closePopup();
			window.location = 'http://pixopolis.throwawaygames.ca/room/'+data.data.roomName
		}
		else
		{
			popup.setMsg("error", data.msg)
		}
        }
      });

    });
    popup.on("registerBtn", "submit", function(e){
      //e.preventDefault();

    });
}

function getRoomList()
{
	if (pixelGrid.roomName === undefined || pixelGrid.roomName === null)
		return false;

    	var inputs = [];

    inputs.push({
	"id":"newRoom",
	"className":"tripple",
	"name":"newRoom",
	"type":"button",
	"readonly":"readonly",
	"value":"Make New Room"
    });

	$.ajax({
		type:"get",
		url:"/rooms",
		dataType:"json",
		error:function(){
			
		},
		success:function(data, status, xhr){
			if (data.status == "success")
			{
				for (x in data.data)
				{
					var viewName = data.data[x][1];
					if (viewName.length === 0)
						viewName = data.data[x][0];
				    inputs.push({
					"elm":"div",
					"id":"roomWrap"+x,
					"className":"tripple",
					//"type":"button",
					//"readonly":"readonly",
					"dataset":{},
					"innerHTML":"<span id='roomName"+x+"' class='roomName' data-roomname='"+data.data[x][0]+"'>"+viewName+
					"</span><div class='editRoom'><span id='edit"+x+"' data-listid='"+x+"'>edit</span><br><span class='delete' data-listid='"+x+"'>delete</span></div>",
				    });
					inputs[inputs.length-1].dataset.roomname = viewName;
					//console.log(inputs[inputs.length-1].dataset.roomName);
				}
				popup = tagLib.popupWindow(inputs, [], false, 2);
				popup.on("newRoom", "click", function(e){
					$.ajax({
						type:"post",
						cache:false,
						url:"/room",
						//xhr:function(){},
						dataType:"json",
						error:function(){
						  console.log("error")
						},
						success:function(data){
							if (data.status == "success")
							{
								window.location = 'http://pixopolis.throwawaygames.ca/room/'+data.data.roomName
							}
							else if (data.status == "success")
							{
								alert(data.msg);
							}
						}
					});
				});

				for (var x = 0, y = inputs.length; x < y; x++)
				{
				    popup.on("roomWrap"+x, "click", function(e){

					if (e.target.className == "roomName" && e.target.tagName != "INPUT")
						window.location = 'http://pixopolis.throwawaygames.ca/room/'+e.target.dataset.roomname;
				    }, "roomName"+x);
					
				    popup.on("roomWrap"+x, "focusout", function(e){
					
					if (e.target.className == "roomName" && e.target.tagName == "INPUT")
					        var nameElm = document.getElementById("roomName"+e.target.dataset.listid);
					        var tmp = document.createElement("span");
					        tmp.id = nameElm.id
					        tmp.className = nameElm.className;
					        tmp.innerHTML = nameElm.value;
					        tmp.dataset.listid = e.target.dataset.listid;
							console.log(nameElm.innerHTML);
							this.removeChild(nameElm);
							this.appendChild(tmp);
				    }, "roomName"+x);	
					
					popup.on("roomWrap"+x, "click", function(e){
						if (e.target.parentNode.className == "editRoom")
						{
							if (e.target.className == "delete")
							{
								if (pixelGrid.roomName == e.target.parentNode.previousSibling.dataset.roomname)
								{
									alert("You cannot delete a room while you're in it!");
									return false;
								}
								else if (confirm('Are you sure you want to delete "'+e.target.parentNode.previousSibling.innerHTML+'"?'))
								{
									$.ajax({
										type:"delete",
										url:"/room?room="+e.target.parentNode.previousSibling.dataset.roomname,
										dataType:"json",
										success:function(data, status, xhr){
											if (data.status == "success")
											{
												e.target.parentNode.parentNode.parentNode.removeChild(e.target.parentNode.parentNode);
											}
											else if (data.status == "failure")
											{
												alert(data.msg);
											}
						
										}
									});
								}
							}
							else
							{
								var nameElm = document.getElementById("roomName"+e.target.dataset.listid);
								
								var tmp = document.createElement("input");
								tmp.id = nameElm.id
								tmp.type = "text";
								tmp.className = nameElm.className;
								tmp.value = nameElm.innerHTML;
								tmp.dataset.listid = e.target.dataset.listid;
								console.log(nameElm.innerHTML);
								this.removeChild(nameElm);
								this.appendChild(tmp);
								tmp.focus();
							}
						}
					}, "edit"+x);
					popup.on("roomWrap"+x, "change", function(e){
		    			if (e.target.className == "roomName")
						{
							var elm = this;
						    var nickName = document.getElementById("roomName"+e.target.dataset.listid).value;
						    console.log(e.target.parentNode.dataset);
							$.ajax({
								type:"post",
								cache:false,
								url:"/roomName",
								data:{"room":e.target.parentNode.dataset.roomname, "nickname":nickName},
								dataType:"json",
								error:function(){
								  console.log("error")
								},
								success:function(data){
									if (data.status == "success")
									{}
									else if (data.status == "success")
									{alert(data.msg);}
								}
							});	
						}
					}, "roomName"+x);					
				}
			}
			else if (data.status == "failure")
			{
				popup = tagLib.popupWindow(inputs, [], false, 2);
				popup.on("newRoom", "click", function(e){
					$.ajax({
						type:"post",
						cache:false,
						url:"/room",
						//xhr:function(){},
						dataType:"json",
						error:function(){
						  console.log("error")
						},
						success:function(data){
							if (data.status == "success")
							{
								window.location = 'http://pixopolis.throwawaygames.ca/room/'+data.data.roomName
							}
							else if (data.status == "success")
							{
								alert(data.msg);
							}
						}
					});	
				});
			}
		}
	}, "json");

}

function showRoom()
{
	if (pixelGrid.roomName === undefined || pixelGrid.roomName === null)
		return false;

    var inputs = [];

    inputs.push({
	"id":"roomName",
	"name":"roomName",
	"type":"text",
	"readonly":"readonly",
	"value":"http://bitgraffiti.com/room/"+pixelGrid.roomName,
    });
	popup = tagLib.popupWindow(inputs, [], false);
	popup.setMsg("message", "Share this link with a up to 3 friends!")

}



function about()
{
	popup = tagLib.popupWindow([], [], false, 2);
	var str = "<h3>What is it?</h3>"+
		"Bitgraffiti.com is a simple online pixel art platform that lets you and your friends draw in your browser at any time.  Bitgraffiti.com is aimed at giving everybody with a browser and an internet connection the ability to easily create pixel art on the fly."+
		"<h3>Beta!</h3>"+
		"Bitgraffiti is currently in the beta phase, which means we are still ironing out the bugs and adding new stuff. So don’t be discouraged if something doesn’t work or is missing. We're on it!  Please provide us with feedback if you have troubles using the site. Be sure to give as detailed a description of any errors you may encounter. <a href='http://throwawaygames.ca/connect.php'>You can contact us here.</a>"+
		"<h3>Browsers!</h3>"+
		"This site is built using HTML5, and as such requires an up to date browser.  Expect the best results with Chrome, but current versions of Firefox and Internet Explorer should still work."+
		"<h3>Like It!</h3>"+
		"Do you enjoy bitgraffiti? Let us know by liking us on Facebook, following us on Twitter and spread some pixel love!"+
		"<h3>Enjoy!</h3>";

	popup.setMsg("message", str)

}

function help()
{
	popup = tagLib.popupWindow([], [], false, 2);
	var str = "<h3>How do I play?</h3>"+
		"Use the colour picker below to choose what color you want to use.  Then use the tools draw or erase pixels, move the canvas, or zoom in and out.  Colours you use get added to your pallet as you change them, so you can easily reuse your favourite colours."+
		"<h3>How do I play with my friends?</h3>"+
		"Click the \"Invite\" button at the top to get the link for the current room.  Share that link with your friends to have them join you in making pixel art."+
		"<h3>I want to continue my drawing later, how do I save?</h3>"+
		"Everything you do is saved automatically.  If you want to go back to a room you've made, click on the \"Rooms\" button at the top for a list of all the rooms you've made."+
		"<h3>How do I make a room?</h3>"+
		"A new room is made for you when you register and log in.  If you are already in a room and want to make a new one, click on the \"Rooms\" button at the top, then choose \"Make New Room\"."+
		"<h3>I'm being told that my pixels aren't being saved!  What do I do?</h3>"+
		"Firstly, please make sure your browser is up to date.  We do not support older browsers, such as Internet Explorer 8 or Firefox 3.5.<br>Slow internet connections can cause some problems with bitgraffiti.  If you are having troubles drawing, try refreshing your browser, or logging out then back in again.";
	popup.setMsg("message", str)

}

function checkPass(input) {
  if (input.value != document.getElementById('password').value) {
    input.setCustomValidity('The two passwords must match.');
  } else {
    // input is valid -- reset the error message
    input.setCustomValidity('');
  }
}

function checkEmail(input) {
  if (input.value != document.getElementById('email').value) {
    input.setCustomValidity('The two emails must match.');
  } else {
    // input is valid -- reset the error message
    input.setCustomValidity('');
  }
}
