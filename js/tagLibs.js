window.tagLib = (function () {

   var tagLib = function(){
     this.test = true;
   };


  //inputs = {id, name, type}
  tagLib.prototype.popupWindow = function(_inputs, _labels, _ajax, _size){
    obj = this;

    if (_inputs === undefined)
      return false;
    if (_labels === undefined)
      _labels = [];

    if (_ajax === undefined)
      _ajax = false;
    if (_size === undefined)
      _size = 1;

    return new popup(_inputs, _labels, _ajax, _size);

  }

  function popup(_inputs, _labels, _ajax, _size)
  {
    var obj = this;//popup object

	if (document.getElementsByClassName("popupBox").length !== 0)
		return false;
		

    obj.dimDiv = document.createElement("div");
    obj.popup = document.createElement("div");
    obj.form = document.createElement("form");
    obj.errMsg = document.createElement("div");
    obj.msg = document.createElement("div");
    obj.inputs=[];
    obj.labels=[];
    obj.events = {};
    obj.dimDiv.id = "popupBackground";
    obj.popup.className = "popupBox";
	if (_size === 2)
		obj.popup.className += " popupBoxMedium";

    for (i = 0; i < _labels.length; i++)
    {
      obj.label[i] = document.createElement("label");
      for (x in _labels[i])
        obj.labels[i].x = _labels[i].x;

    }

	for (i = 0; i < _inputs.length; i++)
	{
		if (_inputs[i].elm === undefined)
			_inputs[i].elm = "input";
		obj.inputs[i] = document.createElement(_inputs[i].elm);
		for (x in _inputs[i])
		{
			if (x !== "elm"){
				if (x === "dataset")
				{
					for (d in _inputs[i][x])
						obj.inputs[i].dataset[d] = _inputs[i][x][d];
				}
				else
					obj.inputs[i][x] = _inputs[i][x];
			}
		}
		//console.log(obj.inputs[i].dataset;
		//console.log(_inputs[i].dataset);
		obj.form.appendChild(obj.inputs[i]);
	}
    obj.popup.appendChild(obj.form);
    obj.popup.appendChild(obj.errMsg);
    obj.popup.appendChild(obj.msg);
    document.body.appendChild(obj.dimDiv);
    document.body.appendChild(obj.popup);

	obj.dimClickFunc = function(e){
		obj.closePopup();
	};
	obj.dimDiv.addEventListener("click", obj.dimClickFunc);
    

    obj.on("form", "submit", function(e){
      if (_ajax)
      {
        e.preventDefault();
        return false;
      }
    });

  }

	popup.prototype.disableClickOff = function()
	{
		var obj = this;
		if(obj.dimDiv !== undefined)
			obj.dimDiv.removeEventListener("click", obj.dimClickFunc);

	}

  popup.prototype.on = function(_id, _type, _listener, _child)
  {
    obj = this;
	if (_child === undefined)
		_child = null;
    if (_id === "form")
    {
      if (obj.events[_id] === undefined || obj.events[_id].type !== _type)
      {
        if (obj.events[_id] instanceof Array)
          obj.events[_id].push({"type":_type, "listener":_listener});
        else
          obj.events[_id] = [{"type":_type, "listener":_listener}];
      }
      else if (obj.events[_id].type === _types && obj.events[_id] !== _listener)
      {
        obj.off(_id, _type, _listener);
        obj.events[_id].push({"type":_type, "listener":_listener});
      }

      eId = obj.events[_id].length-1;

      obj.form.addEventListener(obj.events[_id][eId].type, obj.events[_id][eId].listener);
      return true;
    }


    for (i = 0; i < obj.inputs.length; i++)
    {
      if (obj.inputs[i].id === _id)
      {
        if (obj.events[_id] === undefined || obj.events[_id].type !== _type)
        {
          if (obj.events[_id] instanceof Array)
            obj.events[_id].push({"type":_type, "listener":_listener, "child":_child});
          else
            obj.events[_id] = [{"type":_type, "listener":_listener, "child":_child}];
        }
        else if (obj.events[_id].type === _types && obj.events[_id] !== _listener)
        {
          obj.off(_id, _type, _listener);
          obj.events[_id].push({"type":_type, "listener":_listener, "child":_child});
        }

        eId = obj.events[_id].length-1;
	//if (_child === null)
        	obj.inputs[i].addEventListener(obj.events[_id][eId].type, obj.events[_id][eId].listener);
	//else{
		//obj.form.addEventListener(obj.events[_child][eId].type, obj.events[_child][eId].listener);
	//}
        return true;
      }
    }

    return false;


  }

  popup.prototype.off = function(_id, _type, _listener, _child)
  {
    obj = this;
	if (_child === undefined)
		_child = null;

    if (_id === "form")
    {
      for (j = 0; j < obj.events[_id].length; j++)
      {
        if (obj.events[_id][j].type == _type)
        {
          obj.form.removeEventListener(_type, obj.events[_id]);
          obj.events[_id].splice(obj.events[_id].indexOf(j), 1);
          return true;
        }
      }
      return false;
    }


    for (i = 0; i < obj.inputs.length; i++)
    {
      if (obj.inputs[i].id === _id)
      {
        for (j = 0; j < obj.events[_id].length; j++)
        {
          if (obj.events[_id][j].type == _type)
          {
		if (_child !== null)
			obj.inputs[i].removeEventListener(_type, obj.events[_child]);
		else
			obj.form.removeEventListener(_type, obj.events[_id]);

            obj.events[_id].splice(obj.events[_id].indexOf(j), 1);
            return true;
          }
        }
        return false;
      }
    }
  }

  popup.prototype.closePopup = function()
  {
    obj = this;
    document.body.removeChild(obj.popup);
    delete obj.popup;
    document.body.removeChild(obj.dimDiv);
    delete obj.dimDiv;

    for (i = 0; i < obj.inputs.length; i++)
    {
        _id = obj.inputs[i].id;
        if (obj.events[_id] === undefined)
          break;
        for (j = 0; j < obj.events[_id].length; j++)
        {
          obj.inputs[i].removeEventListener(obj.events[_id][j].type, obj.events[_id][j].listener);
        }
        delete obj.events[_id];
    }

    obj.inputs = [];
    obj.labels = [];
  }

	popup.prototype.setMsg = function(_type, _msg)
	{
		obj = this;
		if (_type == "error")
		{
			obj.errMsg.className = "errorMsg";
			obj.errMsg.innerHTML = _msg;
		}
		else if (_type == "message" && obj.msg !== undefined)
		{
			obj.msg.className = "msg";
			obj.msg.innerHTML = _msg;
		}
		
	}

	popup.prototype.clearMsg = function(_type)
	{
		obj = this;
		if (_type == undefined)
		{
			obj.errMsg.className = "";
			obj.errMsg.innerHTML = "";
			obj.msg.className = "";
			obj.msg.innerHTML = "";
		}
		else if (_type == "error")
		{
			obj.errMsg.className = "";
			obj.errMsg.innerHTML = "";
		}
		else if (_type == "message" && obj.msg !== undefined)
		{
			obj.msg.className = "";
			obj.msg.innerHTML = "";
		}
		
	}

   return new tagLib;
}());



