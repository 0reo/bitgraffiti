function Pixel()
{
	var obj = this;

	obj.settings = {
		"width":1,
		"height":1,
		"x":0,
		"y":0,
		"r":255,
		"g":0,
		"b":0,
		"a":1
	};

}

Pixel.prototype.getColor = function()
{
	var obj = this;
	return obj.settings.r+","+obj.settings.g+","+obj.settings.b+","+obj.settings.a;
}

Pixel.prototype.setColor = function(_col)
{
	var obj = this;
	obj.settings.r = _col.r;
	obj.settings.g = _col.g;
	obj.settings.b = _col.b;
	obj.settings.a = _col.a;
}

Pixel.prototype.getWidth = function()
{
	var obj = this;
	return obj.settings.width;
}

Pixel.prototype.getHeight = function()
{
	var obj = this;
	return obj.settings.height;
}

Pixel.prototype.getPosX = function()
{
	var obj = this;
	return parseInt(obj.settings.x);
}

Pixel.prototype.getPosY = function()
{
	var obj = this;
	return parseInt(obj.settings.y);
}

Pixel.prototype.setPosX = function(_x)
{
	var obj = this;
	obj.settings.x = _x;
}

Pixel.prototype.setPosY = function(_y)
{
	var obj = this;
	obj.settings.y = _y;
}

Pixel.prototype.setDbId = function(_id)
{
	this.dbId = _id;
}
