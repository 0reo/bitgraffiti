<!DOCTYPE html>
<html>
	<head>
		<title>Pixopolis</title>
		<meta charset="utf-8" />
		<link rel="stylesheet" type="text/css" href="/css/main.css"/>
		<script type="text/javascript" src="/js/pixel.js"></script>
		<script type="text/javascript" src="/js/grid.js"></script>
		<script type="text/javascript" src="/js/main.js"></script>
    <script type="text/javascript" src="/js/tagLibs.js"></script>

		<link rel="stylesheet" type="text/css" href="/css/spectrum.css">
		<script type="text/javascript" src="/js/jquery-1.9.1.js"></script>
		<script type="text/javascript" src="http://arshaw.com/xdate/downloads/0.8/xdate.js"></script>
		<script type="text/javascript" src="/js/spectrum.js"></script>
		<script type="text/javascript" src="/js/prefixfree.min.js"></script>


	</head>
	<body>
	<header>
		<nav>
			<ul>
				<li id="invite">Invite</li>
				<!--<li id="share">Share</li>
				<li id="export">Export</li>-->
			        <li id="about">About</li>
				<li id="login">Log In</li>
			</ul>
		</nav>
	</header>
<div id="userspace"></div>
  <canvas id="pixelScape" width="800" height="500" ></canvas>
  <canvas id="checkers" width="800" height="500" ></canvas>
		
	</body>
	<footer>
		<menu type="toolbar" id="pickerIcons">
			<menuitem type="radio" label="draw" icon="" radiogroup="actions" id="draw" class="active">draw
			</menuitem><menuitem type="radio" label="erase" icon="" radiogroup="actions" id="erase">erase
			</menuitem><menuitem type="radio" label="select" icon="" radiogroup="actions" id="move">move
			</menuitem><menuitem type="radio" label="zoomOut" icon="" radiogroup="actions" id="zoomIn">Zoom In
			</menuitem><menuitem type="radio" label="zoomOut" icon="" radiogroup="actions" id="zoomOut">Zoom Out
			</menuitem>
			<!--<menuitem type="radio" label="eyedrop" icon="" radiogroup="actions" id="eyedrop">eyedrop</menuitem>-->
		</menu>
		<input type='text' id="colorPickerInput" />
		<div class="greyBtn" id="bgToggle">Toggle Checkers</div>
		<div id="bottom">
			&copy; 2013 Throw Away Games Inc.
			<div class="socialIcon" id="googlePlus"><a href="https://plus.google.com/102687721328425808205/posts"></a></div>
			<div class="socialIcon" id="twitter"><a href="http://twitter.com/ThrowAwayGames"></a></div>
			<div class="socialIcon" id="facebook"><a href="http://www.facebook.com/ThrowAwayGames"></a></div>
		</div>
	</footer>
</html>
