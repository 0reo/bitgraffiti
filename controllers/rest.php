<?php
	require("./models/users.php");
	require("./models/pixels.php");
	require("./models/rooms.php");

//put when you have an id already
//post when you don't yet have an id
//get to retrieve data
//delete to delete data

$uri = explode("/", substr(@$_SERVER['REQUEST_URI'], 1));
$function = $uri[0];
$function = trim($function, "/");
$function = explode("?", $function);
$function = $function[0];

$method = $_SERVER['REQUEST_METHOD'];
//$data = explode("/", substr(@$_SERVER['REQUEST_URI'], 1));
$data = $uri;
array_shift($data);

$pixelModel = new PixelModel();
$userModel = new UserModel();
switch ($method) {
  case 'PUT':
    echo "put";
    return true;
    exit;
    break;
  case 'POST':
    //if (isset($_POST['register']) && register($_POST, $mysqli))
	if ($function == "register")
	{
		if($userModel->register($_POST, $mysqli))
		{
			list($roomName, $roomId) = joinRoom($mysqli);
			$_SESSION['roomName'] = $roomName;
			$_SESSION['roomId'] = $roomId;
			response("success", "", array("roomName"=>$roomName));
			$userArr = array("what"=>"userEnter", "user"=>$_SESSION);
			sendLongPollPub(json_encode($userArr));
		}
		else
		{
			//if ($mysqli->error)
			response("failure", "Could not register - ".$mysqli->error);
		}
	}
    elseif ($function == "login")
    {
	$hashed_password = crypt($_POST['password'], '$6$rounds=5000$'.AUTH_SALT.'$');
	if ($userModel->login($mysqli, $_POST['email'], $hashed_password))
	{
		list($_SESSION['roomName'], $_SESSION['roomId']) = joinRoom($mysqli);
		response("success", "", array("userId"=>$_SESSION['user_id'], "userName"=>$_SESSION['user_name'], "roomName"=>$_SESSION['roomName']));
		$userArr = array("what"=>"userEnter", "user"=>$_SESSION, "f"=>"l");
		sendLongPollPub(json_encode($userArr));
	}
	else
		response("failure", "Could not log in, please try again");
    } 
    elseif ($function == "logout")
    {
	sendLongPollPub(json_encode($userArr));
	leaveRoom($mysqli);
	$userArr = array("what"=>"userLeave", "user"=>$_SESSION);
	if ($userModel->logout($mysqli))
	{
		response("success");
	}
	else
		response("failure", "Could not log out, please try again");
    } 
	elseif ($function == "draw")
	{
		if(!$userModel->checkLogin($mysqli))
		{
			response("nli", "not logged in");
			return false;
		}
		$pId = $pixelModel->savePixel($_POST, $mysqli);
		if ($pId)
		{
			response("success", '', $pId);
			$pixArr = array("what"=>"pixel", "settings"=>$_POST, "dbId"=>"$pId");
			sendLongPollPub(json_encode($pixArr));
		}
		else
			response("failure", "Could not draw pixel, please try again - ".$mysqli->error);
	}
	elseif($function == "room")
	{
		list($roomName, $roomId) = joinRoom($mysqli);
		//	$_SESSION['roomName'] = $roomName;
		//	$_SESSION['roomId'] = $roomId;
		if ($roomName)
			response("success", "", array("roomName"=>$roomName));
		else
			response("failure", "Could not create new room.  Please try again.");

	}
	elseif($function == "roomName")
	{
		if(nicknameRoom($mysqli, $_POST['room'], $_POST['nickname']))
			response("success", "", array("roomName"=>".."));
		else
			response("failure", "Could not name room.  Please try again.");
	}	
    else
      echo json_encode(array("status"=>"failure"));

    return true;
    exit;
    break;
  case 'GET':
	if (empty($_GET) && empty($_SERVER['HTTP_X_REQUESTED_WITH']))
	{
		include ("./views/main.php");
	}
	elseif ($function == "checkLogin")
	{
		if ($userModel->checkLogin($mysqli))
		{
			$userData = array("userId"=>$_SESSION['user_id'], "userName"=>$_SESSION['user_name'], "roomName"=>$_SESSION['roomName']);
			response("success", "", $userData);
			list($_SESSION['roomName'], $_SESSION['roomId']) = joinRoom($mysqli, $_SESSION['roomName']);
			$userArr = array("what"=>"userEnter", "user"=>$_SESSION, "f"=>"c");
			sendLongPollPub(json_encode($userArr));
		}
		else
		{
			response("failure", "Could not check log in, please try again", $pixelModel->countPixels($mysqli));
		}
	}
	elseif($function == "users")
	{
		$userData = getRoomUsers($mysqli, $_GET['roomName']);
		if ($userData)
		{
			response("success", "", $userData);
		}
		else
		{
			response("failure", "no users found - ".$mysqli->error);
		}
	
	}
	elseif($function == "pixel")
	{
		$pixels = $pixelModel->getPixels($mysqli, $_GET);

		if ($pixels)
		{
			$out = array();
			foreach ($pixels as $pixel)
			{
				$tmpArr = array();
				$tmpArr['dbId']=$pixel['id'];
				foreach ($pixel as $key=>$value)
				{
					if ($key !== "id")
					$tmpArr['settings'][$key]=$value;
				}
				array_push($out, $tmpArr);
			}
			response("success", "", $out);
			

		}
		else
			response("failure", "Could not retrieve pixels, please try again");
	}
	elseif ($function == "rooms")
	{
		$rooms = $userModel->getRooms($mysqli);
		if ($rooms)
		{
			response("success", "", $rooms);
		}
		else
			response("failure", "You have no rooms saved.  Why don't you make one?");
	}

	if ($function == "room" && count($data))
	{
		list($roomName, $roomId) = joinRoom($mysqli, $data[0]);

		if ($roomName)
		{
			$_SESSION['roomName'] = $roomName;
			$_SESSION['roomId'] = $roomId;
			response("success");
			//$userArr = array("what"=>"userEnter", "user"=>$_SESSION, "f"=>"r");
			//sendLongPollPub(json_encode($userArr));
		}
		else
			response("failure", "Could not join room ".$data[0].", please try again");
	} 

    return true;
    exit;
    break;
  case 'HEAD':
    echo "head";
    return true;
    exit;
    break;
  case 'DELETE':
	if ($function == "userRoom")
	{
		if (leaveRoom($mysqli))
		{
			response("success");
			$userArr = array("what"=>"userLeave", "user"=>$_SESSION);
			sendLongPollPub(json_encode($userArr));
		}
		else
			response("failure", "Could not leave room ".$data[0]);
	} 
	elseif ($function == "pixel")
	{
		if ($pixelModel->deletePixel($_GET, $mysqli))
		{
			response("success");
			$pixArr = array("what"=>"delete", "settings"=>$_POST, "dbId"=>$_GET['id']);
			sendLongPollPub(json_encode($pixArr));
		}
		else
			response("failure", "Could not delete pixel - ".$mysqli->error);	
	}
	if ($function == "room")
	{
		if (deleteRoom($mysqli, $_GET['room']))
		{
			response("success");
		}
		else
			response("failure", "Could not delete room ".$_GET['room']);
	}
	return true;
	exit;
	break;
  case 'OPTIONS':
    echo "options";
    return true;
    exit;
    break;
  default:

  break;
}

function response($status, $msg = "", $data = null)
{
	if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']))
	{
		echo json_encode(array("status"=>$status, "msg"=>$msg, "data"=>$data));
	}
}

function sendLongPollPub($data)
{
	$url = BASE_URL.'pub?id='.$_SESSION['roomName'];

	// use key 'http' even if you send the request to https://...
	$options = array(
	    'http' => array(
		'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
		'method'  => 'POST',
		'content' => $data,
	    ),
	);
	$context  = stream_context_create($options);
	$result = file_get_contents($url, false, $context);

	return $result;
}

function getLongPollSub()
{
	$url = BASE_URL.'sub/'.$_SESSION['roomName'];

	// use key 'http' even if you send the request to https://...
	$options = array(
	    'http' => array(
		'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
		'method'  => 'POST'
	    ),
	);
	$context  = stream_context_create($options);
	$result = file_get_contents($url, false, $context);

	return $result;
}
