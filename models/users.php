<?php
session_start();

class UserModel
{
	function register($data, $_mysqli)
	{
		if (!isset($data['username']))
			return false;
		$username = $data['username'];
		if (!isset($data['password']))
			return false;
		$password = $data['password'];
		if (isset($data['email']))
			$email = $data['email'];
		else
			$email = null;

		$username = safe_var($_mysqli, $username);

		$hashed_password = crypt($password, '$6$rounds=5000$'.AUTH_SALT.'$'); // let the salt be automatically generated
		$cookie_auth= uniqid() . $email;
		$auth_key = hash("sha256", $cookie_auth);//64	

		$sql = "INSERT INTO `users` (`username`, `password`, `email`, `authKey`) VALUES ('$username', '$hashed_password', '$email', '$auth_key')";
	
		if ($_mysqli->query($sql))
		{
			setcookie("bgr_auth_key", $auth_key, time() + 60 * 60 * 24 * 7, "/", COOKIE_BASE_URL, false, true);
			// Assign variables to session
			session_regenerate_id(true);

			$_SESSION['user_id'] = $_mysqli->insert_id;
			$_SESSION['user_name'] = $username;
			$_SESSION['user_lastactive'] = time();
			$_SESSION['auth'] = $auth_key;
			return $_mysqli->insert_id;
		}
		else
		{
			return false;
		}

	}

	function login($_mysqli, $email, $password, $remember = true) 
	{

		$email = safe_var($_mysqli, $email);

		$sql = "SELECT * FROM users WHERE password = '" . $password . "' AND email = '" . $email . "' LIMIT 1";
		$result = $_mysqli->query($sql);
		// If there are no matches then the email and password do not match
		if ($result)
	    	{
			while($u = $result->fetch_array())
			{ 
				// Check if user wants account to be saved in cookie
				if($remember)
				{
				    // Generate new auth key for each log in (so old auth key can not be used multiple times in case 
				    // of cookie hijacking)
				    $cookie_auth= uniqid() . $email;
				    $auth_key = hash("sha256", $cookie_auth);//64
				    $auth_query = $_mysqli->query("UPDATE users SET authKey = '" . $auth_key . "' WHERE email = '" . $email . "'");
		 
				    setcookie("bgr_auth_key", $auth_key, time() + 60 * 60 * 24 * 7, "/", COOKIE_BASE_URL, false, true);
				}
				// Assign variables to session
				session_regenerate_id(true);
				$session_id = $u['id'];
				$session_username = $u['username'];
		 
				$_SESSION['user_id'] = $session_id;
				$_SESSION['user_name'] = $session_username;
				$_SESSION['user_lastactive'] = time();
				$_SESSION['auth'] = $auth_key;
				return true;
			}
		}
		else
		{
			return false;
		}
	}  

	function checkLogin($_mysqli)
	{ 
		// Check that cookie is set
		if(isset($_COOKIE['bgr_auth_key']))
		{
			$auth_key = safe_var($_mysqli, $_COOKIE['bgr_auth_key']);
	 
		    	if(!isset($_SESSION['user_name']))
			{
				// Select user from database where auth key matches (auth keys are unique)
				$sql = "SELECT email, password FROM users WHERE authKey = '" . $auth_key . "' LIMIT 1";
				$result = $_mysqli->query($sql);
				if ($result)
				{
					while($u = $result->fetch_array())
					{
						// Go ahead and log in
						//echo $sql;
						return $this->login($_mysqli, $u['email'], $u['password'], true);
					}
				}
				else
				{
					// If auth key does not belong to a user delete the cookie
					setcookie("bgr_auth_key", "", time() - 3600);
					return false;
				}
			}
			else
			{
				if ($_SESSION['auth'] === $_COOKIE['bgr_auth_key'])
					return true;
				setcookie("bgr_auth_key", "", time() - 3600);
				return false;
			}
		}
	}
	 

	function logout($_mysqli)
	{
		// Need to delete auth key from database so cookie can no longer be used
		$username = $_SESSION['user_name'];
		setcookie("bgr_auth_key", "", time() - 3600);
		$sql = "UPDATE users SET authKey = 0 WHERE username = '" . $username . "'";
		// If auth key is deleted from database proceed to unset all session variables
		if ($_mysqli->query($sql))
		{

			unset($_SESSION['user_id']);
			unset($_SESSION['user_name']);;
			unset($_SESSION['user_lastactive']);
			unset($_SESSION['auth']);
			session_unset();
			@session_destroy(); 
			return true;
		}
		else
		{
			return false;
		}
	}
 
 
	// Check if session is still active and if it keep it alive
	function keepalive()
	{
		// If session is supposed to be saved or remembered ignore following code$_mysqli
		if(!isset($_COOKIE['bgr_auth_key']))
		{
			$oldtime = $_SESSION['user_lastactive'];
			if(!empty($oldtime))
			{
				$currenttime = time();
                		// this is equivalent to 30 minutes
				$timeoutlength = 30 * 600;
				if($oldtime + $timeoutlength >= $currenttime){ 
					// Set new user last active time
					$_SESSION['user_lastactive'] = $currenttime;
				}
				else
				{
					// If session has been inactive too long logout
					$this->logout();
				}
			}
		}
	}

	public function getRooms($_mysqli)
	{
		$sql = "SELECT name, nickname FROM `rooms` WHERE userId = ".$_SESSION['user_id']." ORDER BY created desc";

		$result = $_mysqli->query($sql);
	
		if ($result)
			return $result->fetch_all();
		return false;
	}
}



   function safe_var($_mysqli, $var)
  {
      $var = stripslashes($var);
      $var = trim($var);
      $var = strip_tags($var);
      $var = $_mysqli->real_escape_string($var);
      $var = htmlspecialchars($var);
      $var = nl2br($var);

      return $var;
  }
