<?php

    function makeRoom($_mysqli, $userId, $key = '')
    {

      $charset = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

	$key = substr($key, 0, 10);

      for($i=strlen($key); $i<10; $i++)
        $key .= $charset[(mt_rand(0,(strlen($charset)-1)))];

	$key = safe_var($_mysqli, $key);
	$userId = safe_var($_mysqli, $userId);
      $sql = "INSERT INTO `rooms` (`name`, `userId`) VALUES ('$key', '$userId')";
	//echo $sql;
      if ($_mysqli->query($sql))
        return array("id"=>$_mysqli->insert_id, "roomName"=>$key);
      else
        return false;

      //select * from rooms join roomusers where roomusers.roomId =

    }

	function joinRoom($_mysqli, $roomName = false)
	{
		if (!$roomName)
		{
			$tmpArr = makeRoom($_mysqli, $_SESSION['user_id']);
			$roomName = $tmpArr['roomName'];
			$select = $tmpArr['id'];
			if (!$select)
			{
				return false;
			}
		}
		else
		{
			$roomName = safe_var($_mysqli, $roomName);
			$result = $_mysqli->query("SELECT count(`id`) as count FROM `rooms` where `name` = '$roomName'");
			$data=$result->fetch_assoc();
			if ($data['count'] == 1)
				$select = "(SELECT `id` FROM `rooms` where `name` = '$roomName')";
			else
			{
				$tmpArr = makeRoom($_mysqli, $_SESSION['user_id'], $roomName);
				$roomName = $tmpArr['roomName'];
				$select = $tmpArr['id'];
				if (!$select)
				{
					return false;
				}
			}
			
		}

		//$sql = "INSERT IGNORE INTO `roomUsers` (roomId, userId) VALUES ($roomId, ".$_SESSION['user_id'].")";
		
		$sql = "INSERT IGNORE INTO `roomUsers` (roomId, userId) VALUES ($select, ".$_SESSION['user_id'].")";
		
		if ($_mysqli->query($sql))
			return array($roomName, $select);
		else
			return false;

	}

	function getRoomUsers($_mysqli, $roomName)
	{
		$roomName = safe_var($_mysqli, $roomName);
		$sql = "SELECT users.username, users.id 
			FROM roomUsers
			JOIN rooms ON rooms.id = roomUsers.roomId AND rooms.name = '$roomName'
			JOIN users ON users.id = roomUsers.userId AND users.id != ".$_SESSION['user_id'];
		$result = $_mysqli->query($sql);
		return $result->fetch_all(MYSQLI_ASSOC);
	}

	function leaveRoom($_mysqli)
	{

		$sql = "DELETE FROM `roomUsers` where userId = ".$_SESSION['user_id'];

		return $_mysqli->query($sql);
	}

    function nicknameRoom($_mysqli, $room, $nickname)
    {
		$room = safe_var($_mysqli, $room);
        $nickname = safe_var($_mysqli, $nickname);
        $sql = "UPDATE `rooms` SET `nickname` = '$nickname' WHERE name = '$room' AND userId = '".$_SESSION['user_id']."'";
		$_mysqli->query($sql);
		return $_mysqli->affected_rows;
    }
	
	function deleteRoom($_mysqli, $room)
	{
		$sql = "DELETE FROM `rooms` WHERE name = '$room' AND userId = '".$_SESSION['user_id']."'";

		return $_mysqli->query($sql);

	}