<?php

//  require_once("./settings/config.php");
class PixelModel
{

  public function savePixel($data, $_mysqli)
  {
	if (empty($data))
		return false;

	unset($data['width']);
	unset($data['height']);

	$columns = implode(", ",array_keys($data));
	$columns .= ", userId, roomId";
	$escaped_values = array_map($mysqli->real_escape_string, array_values($data));
	$values  = implode(", ", $escaped_values);
	$values.= ", ".$_SESSION['user_id'];
	$values.= ", (SELECT `roomId` FROM `roomUsers` WHERE `userId` = ".$_SESSION['user_id'].")";

	$columns = safe_var($_mysqli, $columns);
	$values = safe_var($_mysqli, $values);
	$sql = "INSERT INTO `pixels` ($columns) VALUES ($values)";

	if ($_mysqli->query($sql))
		return str_pad($_mysqli->insert_id, 20, "0", STR_PAD_LEFT);
	else
		return false;
  }

  public function deletePixel($data, $_mysqli)
  {
    if (!isset($data['id']))
      return false;
	$id = safe_var($_mysqli, $data['id']);
    //$sql = "DELETE FROM `pixels` WHERE id=".$data['id'];
	$sql = "UPDATE `pixels` 
	SET deleted=true 
	WHERE roomId = (SELECT `roomId` 
		FROM `roomUsers` 
		WHERE `roomId` = ".$_SESSION['roomId']." 
		AND `userId` = ".$_SESSION['user_id'].") 
	AND id='$id'";
    if ($_mysqli->query($sql))
      return str_pad($_mysqli->insert_id, 20, "0", STR_PAD_LEFT);
    else
      return false;

  }


  public function getPixels($_mysqli, $data)
  {

	$sql = "SELECT * FROM `pixels` where roomId = (SELECT `roomId` FROM `roomUsers` WHERE `userId` = ".$_SESSION['user_id'].") AND deleted = false";
	if (isset($data['userId']))
		$sql .= " and userId = ".safe_var($_mysqli, $data['userId']);
	if (isset($data['time']))
		$sql .= " and created > ".safe_var($_mysqli, $data['time']);

	$result = $_mysqli->query($sql);

	if ($result)
		return $result->fetch_all(MYSQLI_ASSOC);
	else
		return false;
  }

	public function countPixels($_mysqli)
	{
		$sql = "SELECT COUNT(id) as pixels FROM pixels";
		$result = $_mysqli->query($sql);

		if ($result)
			return $result->fetch_all(MYSQLI_ASSOC);
		else
			return false;
	}

}

